#include<stdio.h> 
#include<string.h>
#include <stdlib.h>
#include "md5.h"

void append(char* s, char c);
  
// Taking argument as command line 
int main(int argc, char *argv[])  
{ 
        FILE *rockyou, *hashes;
       
        rockyou = fopen(argv[1], "r"); 
        hashes = fopen(argv[2], "w"); 
       
        char c;
        char password[15];
       
        while ((c = getc(rockyou)) != EOF) {
            
            if(c == '\n'){// end of password
                char *passHash = md5(password, strlen(password));
                fprintf(hashes, "%s", passHash);//hash and add to hashes file
                fprintf(hashes, "%c", '\n');
                memset(password, 0, sizeof password);//clear current password for the next password
                free(passHash);
            }
            else { 
                append(password, c);
            }
        
        }
       
}

void append(char* s, char c) {
        int len = strlen(s);
        s[len] = c;
        s[len+1] = '\0';
}
